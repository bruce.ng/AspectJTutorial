package aspectj;
 
public aspect HelloAspectJ {
 
  // Định nghĩa ra một pointcut.
  // call: gọi method sayHello() của class HelloAspectJDemo
  //pointcut callSayHello(): call(* HelloAspectJDemo.sayHello());
  
	//Sửa lại thành:
	//Có nghĩa là mọi nơi (class, aspectj) gọi sayHello()
	pointcut callSayHello(): call(* *.sayHello());
	
	//Advice
  before() : callSayHello() {
      System.out.println("Before call sayHello");
  }

  //Advice
  after() : callSayHello()  {
      System.out.println("After call sayHello");
      System.out.println();
  }
 
}

/**
 * AspectJ các phiên bản trước kia sử dụng Annotation để mô tả, các phiên bản gần đây mới đưa vào file *.aj.
 * Trong tài liệu này tôi bỏ qua không sử dụng Annotation, vì nó không rõ ràng bằng việc sử dụng file *.aj.
 * Ngoài ra file *.aj có ngữ pháp giống một class, eclipse sẽ thông báo giúp bạn ngữ pháp viết sai.
 * 
 * AspectJ có chút khác biệt với một class thông thường nó thêm vào rất nhiều các từ khóa.
 * Và file có đuôi là *.aj chứ không phải là *.java. Một số từ khóa của AspectJ:

		aspect   pointcut   privileged   call   execution
		initialization   preinitialization   handler   get   set
		staticinitialization   target   args   within   withincode
		cflow   cflowbelow   annotation   before   after   around
		proceed   throwing   returning   adviceexecution   declare
		parents   warning   error   soft   precedence   thisJoinPoint
		thisJoinPointStaticPart   thisEnclosingJoinPointStaticPart
		issingleton   perthis   pertarget   percflow   percflowbelow
	pertypewithin   lock   unlock   thisAspectInstance
 * */
