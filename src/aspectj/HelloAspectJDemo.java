package aspectj;

public class HelloAspectJDemo {
	
	public static void sayHello() {
		System.out.println("Hello");
	}
	
	public static void greeting() {
		
		String name = new String("John");//JoinPoint tại vị trí khởi đầu tạo đối tượng.
		
		sayHello();//JoinPoint tại vị trí gọi method. JoinPoint của PointCut callSayHello()
		
		System.out.print(name);//JoinPoint
	}
	
	public static void main(String[] args) {
		
		sayHello();//JoinPoint tại vị trí gọi method. JoinPoint của PointCut callSayHello()
		
		System.out.println("--------");//JoinPoint
		
		sayHello();//JoinPoint tại vị trí gọi method. JoinPoint của PointCut callSayHello()
		
		System.out.println("--------");//JoinPoint
		
		greeting();//JoinPoint tại vị trí gọi method.
	}
	
	/**
	 * Một class như trên là hoàn toàn không có gì đáng chú ý, tuy nhiên đặt ra vấn đề bạn muốn 
	 * chương trình làm một cái gì đó ngay trước, hoặc ngay sau khi method sayHello() được gọi, 
	 * chẳng hạn in ra màn hình câu thông báo. Theo cách truyền thống bạn sẽ thêm vào một dòng lệnh in 
	 * ra màn hình ngay trước khi gọi method sayHello().
	 * 
	 * http://o7planning.org/web/fe/default/vi/document/18642/aspectj-cho-nguoi-moi-bat-dau-lap-trinh-huong-khia-canh-aop
	 * 
	 * AspectJ là một hướng lập trình khía cạnh, nó cho bạn một giải pháp khác để giải quyết vấn đề này, tất nhiên AspectJ
	 * còn làm được nhiều hơn thế
	 * */
}
