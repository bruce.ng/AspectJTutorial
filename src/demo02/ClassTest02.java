package demo02;

import figures.Point;

public class ClassTest02 {
	
	public static void main(String[] args) {
		
		Point point = new Point(10, 200);
		
		System.out.println("---- (1) ----");
		
		point.move(20, 30);//Đối tượng tham gia (target) . Các tham số (args) ;điểm tham gia (joinPoint)
		
		System.out.println("---- (2) ----");
		
		System.out.println(point.toString());
		
		System.out.println("---- (3) ----");
		
		point.setX(100);
	}
}

/**
target: Là đối tượng tham gia tại JoinPoint (Điểm tham gia), đối tượng gọi method(hoặc đối tượng truy cập field).
args: Là tham số.

 * */
