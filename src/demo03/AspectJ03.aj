package demo03;
//Toán tử && ||
// Chú ý: Phải import FigureElement & Point
import figures.FigureElement;
import figures.Point;

public aspect AspectJ03 {
	
	// Pointcut gồm các hành động di chuyển (move)
	pointcut moveAction() : (
           call(void FigureElement.move(int,int)) ||
           call(void Point.setX(int))             ||
           call(void Point.setY(int))
	) && within (ClassTest03);
	
	before() : moveAction()  {
		System.out.println("before move");
	}
	
}