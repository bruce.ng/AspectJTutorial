package demo01;

public aspect AspectJ01 {
	
	// Class Point và AspectJ này không cùng package vì vậy phải ghi rõ cả package (Bắt buộc).
	// Pointcut này định nghĩa các điểm tham gia (JoinPoint) chỉ trong phạm vi class ClassTest01
	// ClassTest01 và AspectJ này cùng package, vì vậy có thể bỏ qua package trong within.
	pointcut callSetX()  : call(void  figures.Point.setX(int)) && within (ClassTest01) ;
	
	// Advice
	before() : callSetX()  {
		System.out.println("Before call Point.setX(int)");
	}
	
	//Tập hợp các JoinPont gọi method Point.setX(int) với package bất kỳ.
	//pointcut callSetX()  : call(void  *.Point.setX(int)) ;
	
	//Tập hợp các Pointcut gọi Point.setX(int) với package bất kỳ và kiểu trả về bất kỳ.
	//pointcut callSetX()  : call(* *.Point.setX(int)) ;
	
	
	//Pointcut gọi các method tĩnh setX(int) của một class có tên *Point (APoint, AbcPoint,...)
	//và thuộc package sample, kiểu trả về int.
	//pointcut callSetX()  : call(public static int sample.*Point.setX(int)) ;
	
	
	//Sử dụng (..) để mô tả method có 0 hoặc nhiều tham số.
	//pointcut callSetX()  : call(public static int sample.*Point.setX(..)) ;
}

/**
 * Chý ý mô tả class, method, *, ..
 * */

/**
within sử dụng để hạn chế phạm vi của Pointcut trong trường hợp dưới đây Chỉ chứa các điểm tham gia (JoinPoint)
trong class ClassTest01.
- within (ClassTest01)
 
Có thể sử dụng import
import figures.Point;
.....
// Và có thể viết ngắn gọn.
pointcut callSetX()  : call(void  Point.setX(int)) && within (ClassTest01) ;

 * */
